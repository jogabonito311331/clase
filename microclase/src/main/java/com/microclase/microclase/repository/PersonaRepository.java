package com.microclase.microclase.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.microclase.microclase.entity.Personas;


@Repository
public interface PersonaRepository extends JpaRepository<Personas, Integer>{

	@Query(name =   "Personas.getPersonas")
	List<Personas> getPersonas();
	
	@Override
	public List<Personas> findAll();
} 
