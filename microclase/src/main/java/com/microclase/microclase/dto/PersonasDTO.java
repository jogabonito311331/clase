package com.microclase.microclase.dto;

public interface PersonasDTO {

   int getPersona();
   String getNombres();
   String getApellidos();
   String getTelefono();
   String getEmailo();
}
