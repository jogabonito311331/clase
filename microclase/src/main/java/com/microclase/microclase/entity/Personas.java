package com.microclase.microclase.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import lombok.Data;

@NamedNativeQuery(name = "Personas.getPersonas", query = "select * from personas",resultClass = Personas.class)


@Entity
@Data
@Table(name = "personas")
public class Personas {
    @Id
	@Column(name = "id_persona", unique = true)
	private int id_persona;
	
	@Column(name = "nombres")
	private String nombres;
	
	@Column(name = "apellidos")
	private String apellidos;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "email")
	private String email;
}
