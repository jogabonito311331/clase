package com.microclase.microclase.impl;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microclase.microclase.entity.Personas;
import com.microclase.microclase.repository.PersonaRepository;
import com.microclase.microclase.service.PersonaService;



@Service
public class PersonasServiceImpl implements PersonaService{


	@Autowired
	PersonaRepository personaRepository;

	@Override
	public List<Personas> getPersonas() {
		return personaRepository.getPersonas();
	}


	@Override
	public List<Personas> findAll() {
		return personaRepository.findAll();
	}

}
