package com.microclase.microclase.service;

import java.util.List;

import com.microclase.microclase.entity.Personas;


public interface PersonaService {

	List<Personas> getPersonas();
	List<Personas> findAll();

}
